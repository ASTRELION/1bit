# **1bit**
***A Minecraft resource pack generator***

Originally created to generate a x1 Minecraft resource pack, __1bit__ can be used to create templates for any resource pack resolutions from a source resource pack (like the Vanilla assets).

**Example Uses**
- Create a x1 resource pack from Vanilla x16 textures by using their average colors
- Create a high-res x32 resource pack from Vanilla x16 textures
- Create a low-res, performance friendly x8 resource pack from Vanilla x16 textures

# Install & Run

## Prerequisites
- [Aseprite](https://aseprite.org) (~$20)

### *optional* Obtain Vanilla textures
*If you want to use another resource pack as your source, you don't need to do this*
1. Navigate to `C:\Users\<username>\AppData\Roaming\.minecraft\versions`
2. Enter the folder with the Minecraft version of textures you want, e.g. `1.18-pre5/`
3. In a terminal window, run `jar xf jar-file <version>.jar`, e.g. `jar xf jar-file 1.18-pre5.jar`. This will extract all of Minecraft's resources to the current folder
    - *NOTE: the above command with extract ALL files to your current directory, so consider copying the `.jar` file to a new folder*
- *Textures are located in `assets/minecraft/textures/`*

# Features & Use Cases

- Automatically scale source textures to target texture resolution using [RotSprite](https://en.wikipedia.org/wiki/Pixel-art_scaling_algorithms#RotSprite), bilinear, or nearest-neighbour algorithms
- Enable/disable each texture category ('block', 'item', etc.) individually
- Specify resolution targets for each texture category individually
- Generate textures with source texture
- Generate textures with source texture average color
- Generate bordered textures with source texture average color and black borders
- Generate x-ray textures with source texture average color

# Guide

# Donate :)

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/O5O7XRIS)