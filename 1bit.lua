-- 1bit Resource Pack Script
-- Converts vanila Minecraft textures into 1x1x1 textures

if (not app.isUIAvailable) then
    print("Running via CLI...")
end

local sourceRoot = "Z:/files/Personal/Projects/MinecraftResourcePacks/vanilla/assets/minecraft/textures/"

-- Append all elements of table2 to table1 and return it
local function appendTable(table1, table2)
    for _, item in ipairs(table2) do
        table.insert(table1, item)
    end

    return table1
end

local function listFiles(directory)
    local files = {} -- files in this directory

    for _, file in ipairs(app.fs.listFiles(directory)) do
        local filePath = app.fs.normalizePath(app.fs.joinPath(directory, file)) -- full file-path of the current file
        
        -- Add all PNG's in this path
        if app.fs.isFile(filePath) and string.lower(app.fs.fileExtension(filePath)) == "png" then
            table.insert(files, string.sub(filePath, string.len(sourceRoot) + 1))
        -- Search the directory instead
        elseif app.fs.isDirectory(filePath) then
            appendTable(files, listFiles(filePath))
        end
    end

    return files
end

-- Constants --
local DIALOG_TITLE <const> = "1bit Resource Pack Creator"
local RESIZE_METHODS <const> = {
    "RotSprite",
    "Bilinear",
    "Nearest-neighbour",
}
local COLOR_METHODS <const> = {
    "Average",
    "Original"
}
local TRANSPARENCY_THRESHHOLD = 5

-- Aseprite pixelColor getters --
local rgba = app.pixelColor.rgba;
local rgbaA = app.pixelColor.rgbaA;
local rgbaR = app.pixelColor.rgbaR;
local rgbaG = app.pixelColor.rgbaG;
local rgbaB = app.pixelColor.rgbaB;
local graya = app.pixelColor.graya;
local grayaA = app.pixelColor.grayaA;

-- Globals --
local destinationRoot = app.fs.normalizePath(app.fs.joinPath(app.fs.userDocsPath .. "/../", "AppData/Roaming/.minecraft/resourcepacks/1bit/assets/minecraft/textures/"))
local sourceDimension = 16
local textureFiles = listFiles(sourceRoot)
local textureDetailsTable = {
    block       = { enabled = true,     dimension = sourceDimension / 4, },
    colormap    = { enabled = false,    dimension = sourceDimension, },
    effect      = { enabled = false,    dimension = sourceDimension, },
    entity      = { enabled = true,     dimension = sourceDimension, },
    environment = { enabled = true,     dimension = sourceDimension / 2, },
    font        = { enabled = false,    dimension = sourceDimension, },
    gui         = { enabled = false,    dimension = sourceDimension, },
    item        = { enabled = true,     dimension = sourceDimension / 2, },
    map         = { enabled = false,    dimension = sourceDimension, },
    misc        = { enabled = true,     dimension = 1, },
    mob_effect  = { enabled = true,     dimension = sourceDimension / 2, },
    models      = { enabled = true,     dimension = 1, },
    painting    = { enabled = true,     dimension = sourceDimension / 2, },
    particle    = { enabled = true,     dimension = sourceDimension / 2, },
}

if (app.isUIAvailable) then
    -- Dialogs --
    -- Texture settings Dialog
    local textureDialog = Dialog(DIALOG_TITLE)
    -- Texture details Dialog (block - GUI)
    local textureDetailsDialog1 = Dialog(DIALOG_TITLE)
    -- Texture details Dialog (Item - Particle)
    local textureDetailsDialog2 = Dialog(DIALOG_TITLE)

    textureDialog
        :separator{
            text = "Texture Directories",
        }
        :entry{
            id = "sourceRoot",
            label = "Source Texture Directory",
            text = sourceRoot,
            onchange = function ()
                if app.fs.isDirectory(textureDialog.data.sourceRoot) then
                    textureFiles = listFiles(textureDialog.data.sourceRoot)
                    textureDialog:modify{
                        id = "fileCount",
                        text = string.format("%d PNG files", #textureFiles),
                    }
                end
            end,
        }
        :label{
            id = "fileCount",
            text = string.format("%d PNG files", #listFiles(textureDialog.data.sourceRoot)),
        }
        :entry{
            id = "destinationLocation",
            label = "Destination Texture Directory",
            text = destinationRoot,
        }
        :separator{
            text = "Texture Information",
        }
        
        :number{
            id = "sourceDimension",
            label = "Source texture dimension",
            text = "16",
            decimals = 0,
        }
        :combobox{
            id = "resizeMethod",
            label = "Resize Method",
            options = RESIZE_METHODS,
            option = RESIZE_METHODS[1],
        }
        :combobox{
            id = "colorMethod",
            label = "Color Method",
            options = COLOR_METHODS,
            option = COLOR_METHODS[1],
        }
        :separator{}
        :button{
            id = "confirm",
            text = "Confirm",
        }
        :button{
            id = "cancel",
            text = "cancel",
        }
        :show()

    -- Source Root
    sourceRoot = textureDialog.data.sourceRoot
    -- Destination Root
    destinationRoot = textureDialog.data.destinationLocation
    -- Source resolution
    sourceDimension = textureDialog.data.sourceDimension

    if textureDialog.data.cancel then
        do return end
    end

    -- Construct detail dialods
    local i = 0;
    for type, details in pairs(textureDetailsTable) do
        local textureDetailsDialog = textureDetailsDialog1
        
        if (i >= 7) then
            textureDetailsDialog = textureDetailsDialog2
        end

        textureDetailsDialog
            :separator{
                text = "Texture - " .. type:upper()
            }
            :check{
                id = type .. "Enabled",
                label = "Enable " .. type:upper() .. " textures",
                selected = details["enabled"]
            }
            :number{
                id = type .. "Dimension",
                label = "Target " .. type:upper() .. " Dimension",
                text = tostring(details["dimension"]),
                decimals = 0,
            }

        i = i + 1
    end

    textureDetailsDialog1
        :button{
            id = "confirm",
            text = "Confirm"
        }
        :button{
            id = "cancel",
            text = "cancel"
        }
        :show()

    if textureDetailsDialog1.data.cancel then
        do return end
    end

    textureDetailsDialog2
        :button{
            id = "confirm",
            text = "Confirm"
        }
        :button{
            id = "cancel",
            text = "cancel"
        }
        :show()

    if textureDetailsDialog2.data.cancel then
        do return end
    end

    for type, details in pairs(textureDetailsTable) do
        if (textureDetailsDialog1.data[type]) then
            textureDetailsTable.type.details = textureDetailsDialog1.data[type].enabled
        elseif (textureDetailsDialog2.data[type]) then
            textureDetailsTable.type.details = textureDetailsDialog2.data[type].enabled
        end
    end
end

local startTime = os.time()

-- Analyze and create new textures
for _, texturePath in ipairs(textureFiles) do
    -- Skip disabled texture paths
    for type, details in pairs(textureDetailsTable) do
        if (texturePath:find("^" .. type) ~= nil and not details.enabled) then
            goto continue
        end
    end

    -- Open the sprite file (.png)
    local sprite = app.open(app.fs.joinPath(sourceRoot ,texturePath))

    for _, cel in ipairs(sprite.cels) do
        -- new image, forced to RGB color
        local image = Image(cel.image.width, cel.image.height, ColorMode.RGB)

        local avgR = 0  -- average R value
        local avgG = 0  -- average G value
        local avgB = 0  -- average B value
        local avgA = 0  -- average A value
        local rgbN = 1  -- number of (non-transparent) pixels
        local n = 1     -- number of (any) pixels

        -- bounds for the used portion of an image
        local minY = image.height - 1
        local maxY = 0
        local minX = 0
        local maxX = image.width - 1

        -- analyze source texture
        for y = 0, image.height, 1 do
            for x = 0, image.width, 1 do
                local pixelColor = Color(cel.image:getPixel(x, y))

                -- calculate average color
                if pixelColor.alpha > TRANSPARENCY_THRESHHOLD then
                    avgR = avgR + ((pixelColor.red - avgR) / rgbN)
                    avgG = avgG + ((pixelColor.green - avgG) / rgbN)
                    avgB = avgB + ((pixelColor.blue - avgB) / rgbN)
                    rgbN = rgbN + 1

                    minY = math.min(minY, y)
                    maxY = math.max(maxY, y)
                    minX = math.min(minX, x)
                    maxX = math.max(maxX, x)
                end

                avgA = avgA + ((pixelColor.alpha - avgA) / n)
                n = n + 1
            end
        end

        -- only allow blocks to have transparency
        if (texturePath:find("^block") == nil) then
            avgA = 255
        end

        -- paint new texture
        for y = 0, image.height, 1 do
            for x = 0, image.width, 1 do
                local pixelColor = Color(cel.image:getPixel(x, y))

                -- only draw over portion of the image that is used
                if pixelColor.alpha > TRANSPARENCY_THRESHHOLD and 
                y >= minY and y <= maxY and x >= minX and x <= maxX then
                    image:drawPixel(x, y, rgba(avgR, avgG, avgB, avgA))
                else
                    -- replace any borderline transparent pixels with fully transparent pixels
                    image:drawPixel(x, y, rgba(255, 255, 255, 0))
                end
            end
        end
        
        -- Resize textures
        for type, details in pairs(textureDetailsTable) do
            if (texturePath:find("^" .. type) ~= nil) then
                local ratio = math.max(math.floor(sourceDimension / details.dimension), 1)
                image:resize{
                    width = math.max(math.floor(sprite.width / ratio), details.dimension),
                    height = math.max(math.floor(sprite.height / ratio), details.dimension),
                    method = "rotsprite",
                }
                sprite:resize(
                    math.max(math.floor(sprite.width / ratio), details.dimension),
                    math.max(math.floor(sprite.height / ratio), details.dimension)
                )
                break
            end
        end

        -- Set new image
        cel.image = image
    end

    sprite:saveAs(app.fs.joinPath(destinationRoot, texturePath))
    sprite:close()

    ::continue::
end

local durationS = os.difftime(os.time(), startTime)
local durationM = durationS / 60
local totalTimeString = string.format("Total time: %.2f seconds (%.2f minutes)", durationS, durationM)
local averageTimeString = string.format("Avg. time per texture: %.4f seconds", durationS / #textureFiles)

if (app.isUIAvailable) then
    app.alert{
        title = "1bit Run Information",
        text = {
            totalTimeString,
            averageTimeString,
        },
    }
else
    print(
        "Done.\n" ..
        totalTimeString .. "\n" ..
        averageTimeString
    )
    app.exit()
end